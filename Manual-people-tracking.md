## Quick start

Use three terminals:

```bash
cd /home/st/simon
sudo UID="$(id -u)" GID="$(id -g)"  docker compose up
```

```bash
cd /home/st/simon/hand-tracking
poetry run python -m hand_tracking 
```

```bash
cd /home/st/simon/people-tracking-backend
poetry run python -m people_tracking_backend
```

# People Tracking documentation

For first time use in a new location, the following steps should be followed:
1. [Calibration of axis cameras](#calibration-of-the-axis-cameras)
2. [Calibration of daheng cameras](#calibration-of-the-daheng-cameras)
3. [Run the project](#running-the-project)
4. [Test the scene](#setting-up-the-scene)
5. [Filtering detections outside the container](#filtering-detections-outside-the-container) 

For other problems, [Troubleshoot](#troubleshoot).


## Running the project

The people tracking part of the project will run automatically at startup and the visualisation should display people after about 30 seconds. Although, if the whole container has just been started, the cameras may have not been up yet when the containers were launched and the project should be relaunched from `/home/st/simon` with:

```bash
sudo docker compose up
```

Two other parts of the project have to be launched outside the containers for now, the hand detection and the backend linking the people tracking to the application.

First in a terminal, navigate to `/home/st/simon/hand-tracking` and run `poetry run python -m hand_tracking`.

Once the hand tracking is running, on a second terminal navigate to `/home/st/simon/people-tracking-backend` and run `poetry run python -m people_tracking_backend`. 

If everything is working the visualisation of the cameras above the fridges should appear, they should contain two fridges handles in frame with some space to the left and right of these handles visible. To check if they are correctly set up, try grabbing each handle and see if something appears in the terminal of the hand tracking for each handle. If nothing happens, look at [how to calibrate those cameras](calibration-of-the-daheng-cameras).

The project is now running! There are [a few tests](testing-the-setup) you can try make sure everything is running smoothly.

## Setting up the scene

The scene defines the position of the cabinets, their handles and the check-in zones that will be shown on the people tracking visualisation and be used by the tracker and backend to know when to open a door. The scene.json file defining the scene is found in `/home/st/simon/people-tracking-backend/people_tracking_backend/scene.json`.

In this file, each object is defined by mutliple variables:
- id: name of the current object being defined
- reference_frame: the position will be given relative to the id referenced here. Use "global" or "origin" to use absolute coordinates, otherwise the current object position will be computed starting from the object id specified here.
- position: position relative to the reference_frame in meter with x, y, z. x going from the left wall to the right window, y going from the door towards the fridges and z representing the height.
- orientation: orientation relative to the reference_frame with [roll, pitch, yaw](https://support.pix4d.com/hc/en-us/articles/202558969-Yaw-Pitch-Roll-and-Omega-Phi-Kappa-angles)
- color: deprecated, used to be the color for visualisation but made it hard to see the skeletons

There are really two important type of objects in this file. The first are the handles zones, they have to correspond to the correct locations because they are used to open the fridges' doors with people tracking. The second one are the checked-in zones, which defines where every customer must be logged in otherwise the fridges will close themselves. 

To test that the handles zones are properly set up, run the project and visualise that the skeleton arm reaches into the handle zone when you're grabbing a handle door.

To test the checked-in zones, log in with the app and make sure everything unlocks when you enter the zone and closes when you move out of the zone. Test throughly near the entrance and all along the airport fence to make sure the zones finish and start at the correct locations.

Be aware that closing the doors off is currently slower than opening them, which will have to be improved in the future and also that stepping out of the container will log you out. 


## Calibration of the daheng cameras

The calibration of these cameras is both simpler but more tedious to do. The cameras only have to see the handles correctly to work but since working on the full frame is slower, only a crop around each handle is used to track hands, and these crops have to be written manually in the code.

To look the output of the cameras without running the project. The camera software can be found in `/home/st/simon/Galaxy.../bin/GxGalaxyViewer` from which you visualise the frames taken by each cameras. 

The code is not yet finished an easy to change, and will have to be changed manually in `/home/st/simon/hand-tracking/hand_tracking/__main__.py`.

- Crops of the first camera: line 227
- Second camera: l.261
- Third: l.291

The crops give the pixel number defining the area of capture to be used: [x_start, y_start, x_end, y_end]


## Calibration of the axis cameras

The axis cameras are situated on the coners of the container and above the door, they are used for people tracking. Calibration

### Virtual env setup
A virtual environment has been created with conda to run the calibration script, to see the availabel environment type: 
```bash
conda env list 
```

A environment called calibration_env should be visible in the list, if not go to: `/home/st/simon/tooling/calibration` and run 
```bash
conda create --name calibration_env --file requirements.txt 
```

Once the virtual environment is available, activate it with: 
```bash
conda activate calibration_env 
```

The project uses a modified version of the python package gtsam, it should be already installed in the virtual env but since I had issue in the past , we can make sure it is setup correctly by going to `/home/st/simon/gtsam/build/python` and run 
 ```bash
python -m pip uninstall gtsam  # In case gtsam is already installed, first uninstall to ensure an overwrite.
python setup.py install
```

### Setting up the aruco markers

To do the calibration, we use aruco markers that are easily recognised by the calibration software. They should be  in the cabinet of the technical room but of some are missing/damaged they can be generated on the following website: https://chev.me/arucogen/

Make sure to use the Dicionary: 4x4 (50,100,250,1000) and set the marker size to 189mm. Around 20 markers should be plenty. 

Lay these markers evenly on the floor of the container, their orientation and location doesn't matter except to the aruco marker with ID 0. This marker 0 should be placed at the center of the container (where a white tape has been placed), the sheet of paper should be placed with the top side towards the right window of the container and be parallel to the fridges.

To make the calibration even more precise some aruco markers can be placed on some height, f.e. a stool, rather than on the floor.

### Capturing calibration images

Once all the aruco markers have been placed, navigate to `/home/st/simon/tooling/calibration`. The file `cameras.json` lists all the cameras, their ips and resolutions, this file should be already configured for the current setup but can be modified to add/remove cameras. 

To grab the images run: 

```bash
python axis_grab_snapshots.py --output-dir "/home/st/simon/calibration/data/20231129/aruco"
```

Verify then the pictures taken in `/home/st/simon/calibration/data/20231129/aruco`, there should be a folder for each camera. Make sure that the aruco markers are visible by all the cameras.

### Launching the cameras calibration

Before launching the calibration, you have to specify which aruco marker was layed out on the floor or on some height. This can be written in the file `/home/st/simon/calibration/constraints.json`. Which should look something like:
```json
{
    "0.png": {
        "0": "on_floorplane",
        "1": "on_horizontal_plane",
        "2": "ignore"
        }
}
```

- on_floorplane: if the sheet of paper is on the floor
- on_horizontal_plane: if it is laying flat on another surface than the floor
- ignore: don't use that aruco marker for the clalibration

In `/home/st/simon/calibration`, run 
```bash
python  extrinsic_calibration.py --calibration-images-directory /home/st/simon/calibration/data/20231129 --output-directory /home/st/simon/calibration/data/20231129
```

The resulting calibration will be outputed in `/home/st/simon/calibration/data/20231129/extrinsics`, take a look at the output to make sure it makes sense, that the aruco markers are correctly found and that there are not too much projection errors showing up (red lines).

You are now done calibrating the axis cameras.

## Filtering detections outside the container

There are a few edge cases that are not fatal for the project but might introduces unintended if they are not perfectly configured. These are all about making sure each zones is well defined and corresponds to the reality.

To filter out reflection and people outside the container, define how far from the center of the container can people be before not being tracked anymore. The tracker will only be tracking people inside a rectangle around the center of the container, this is defined under: `/home/st/simon/tooling/probabilistic_tracker/config.json` as tracking_filter, giving the position of each side of the area relative to the container's center.

Its values are probably still correct and don't have to be changed but to test it, try to move as close as possible to a wall and see if you disappear from the visualisation. Try the same on each side of the container (or close to the fridges), try also to see how well the limit at the container entry is set up. 

A particularly important point is to have the limit towards the fridges as close to the fridges door as possible, because it will also remove all the detections from reflections on the glass doors. At the same time, we must make sure that customers will not disappear if the openy a fridge and lean a bit into it. 

If the values are incorrect and should be changed, simply modify the value in the json file and restart the docker containers to reload the new config with `cd /home/st/simon && 
sudo docker compose up`.

Making sure this filter well set up as well as the checked-in zones, the project should now be well defined and ready to be used.

# Troubleshoot

## Inference

## Tracker

## Visualisation

## Hand tracking

## People tracking backend
